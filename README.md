# Oi, me chamo Marcus Roza 👋

### Sobre mim

💻 
Sou um desenvolvedor de software Full Stack com experiência sólida em tecnologias modernas. Minha paixão é criar soluções inovadoras e robustas para desafios complexos.

Tenho amplo conhecimento e experiência em Node.js, React e PostgreSQL, o que me permite desenvolver aplicações escaláveis e eficientes. Estou aprimorando meu conhecimento em TypeScript, que utilizo para garantir um código limpo, seguro e de fácil manutenção.

Além disso, possuo habilidades significativas em lógica de programação e resolução de problemas. Tenho proficiência no uso de Git e GitHub para controle de versão e colaboração em equipe.

Trabalhei em diversos projetos desafiadores, nos quais pude aplicar padrões de projeto e arquitetura de software para criar soluções de alta qualidade. Minha experiência inclui a criação e o consumo de APIs RESTful, a manipulação avançada de eventos com a DOM e o desenvolvimento de interfaces de usuário modernas e responsivas.

Estou constantemente buscando aprimorar minhas habilidades técnicas e acompanhar as últimas tendências e melhores práticas do mercado. Sou apaixonado por aprender novas tecnologias e estou sempre aberto a novos desafios e oportunidades.

Tenho um compromisso forte com a qualidade do código e a entrega de resultados excepcionais. Sou um comunicador eficaz e um colaborador proativo em equipes multidisciplinares.

Se você está em busca de um desenvolvedor com habilidades técnicas sólidas, apaixonado por inovação e comprometido com o sucesso do projeto, estou pronto para contribuir e enfrentar novos desafios.

Fique à vontade para entrar em contato comigo para discutir possíveis colaborações ou oportunidades de trabalho. Vamos trabalhar juntos para criar soluções incríveis!

🎓 Certificação pela Cubos Academy  [Desenvolvimento de Software Fullstack](https://cubos.academy/cursos/desenvolvimento-de-software-v2)

🔎 Também sou curioso sobre TypeScript e Arquitetura de codigo

✒️ Eu gosto de no meu tempo livre ficar com a família, assistir doramas e fazer pizza com meu filho;

📚 Lendo: Clean Code - Robert C. Martin

### Eu já trabalhei com... 🔧

**Tecnologias e Ferramentas**

![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E)
![NodeJS](https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white)
![HTML5](https://img.shields.io/badge/html5-%23E34F26.svg?style=for-the-badge&logo=html5&logoColor=white)
![CSS3](https://img.shields.io/badge/css3-%231572B6.svg?style=for-the-badge&logo=css3&logoColor=white)
![React](https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB)
![Postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white)
![Git](https://img.shields.io/badge/git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white)
![GitHub](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white)
![Figma](https://img.shields.io/badge/figma-%23F24E1E.svg?style=for-the-badge&logo=figma&logoColor=white)
![VS Code](https://img.shields.io/badge/VS%20Code-0078d7.svg?style=for-the-badge&logo=visual-studio-code&logoColor=white)


### Eu estou estudando... 🧩

#### Framework:

  ![Angular](https://img.shields.io/badge/angular-%23DD0031.svg?style=for-the-badge&logo=angular&logoColor=white)
  ![NestJS](https://img.shields.io/badge/nestjs-%23E0234E.svg?style=for-the-badge&logo=nestjs&logoColor=white)

#### Biblioteca:

  ![Jest](https://img.shields.io/badge/-Jest-%23C21325?style=for-the-badge&logo=jest&logoColor=white)
  ![Prisma](https://img.shields.io/badge/prisma-%230E1316.svg?style=for-the-badge&logo=prisma&logoColor=white)
  ![Zod](https://img.shields.io/badge/zod-%23008FFF.svg?style=for-the-badge&logo=zod&logoColor=white)

#### Linguagem:

  ![TypeScript](https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white)


### Cursos realizados 🤓


[![DDS Cubos Academy](https://img.shields.io/badge/-DDS%20Cubos%20Academy-19272E?style=for-the-badge&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAOCAYAAAAfSC3RAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAEQSURBVHgBjZNTcNAEIXfrJMoPSAkpwNTAaYCSAfugD0GThzgjOiADhAdhApwB1l+JK6cI3aHGWstbeKNnJFWtsffm3myHyDFzFbOhsfrXU6DKGqiqMZIKRNZizhlIPrGbfWFVdf/xM2VPqfiidxXRLQezqdHg8L9YOUCzEsAXXRdYUVYTnKWBC4DUCvMMDUB7hT3O8NNTugFRoQJdB3gX/eZ7MYejpurP8yXA0a/sfimXZvFZovpYoqtlSHnJ3g4S0WqMRmbVi7tAndOXl568FPOlck0Olh/BYNKj9l6VJjCYrFh8JtuPiT86ANAmP96hKXCjOIZmNl9QWTbNKc1RiqJXNM3NK/tESF3XU6l/gHnFvELsPPX8gAAAABJRU5ErkJggg==)](https://cubos.academy/)
[![Grasshopper](https://img.shields.io/badge/-Grasshopper-FFA800?style=for-the-badge&logo=google&logoColor=white)](https://grasshopper.app/)

### Certificados e Licenças 📜


***Coodesh***

![SQL](https://img.shields.io/badge/SQL-%2300758F.svg?style=for-the-badge&logo=sql&logoColor=white)
![GMAT - Português](https://img.shields.io/badge/GMAT%20-%20Portugu%C3%AAs-%23147EFB.svg?style=for-the-badge&logo=gmatacademic&logoColor=white)
![Git](https://img.shields.io/badge/Git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white)
![JavaScript](https://img.shields.io/badge/JavaScript-%23F7DF1E.svg?style=for-the-badge&logo=javascript&logoColor=black)
![ReactJS](https://img.shields.io/badge/ReactJS-%2361DAFB.svg?style=for-the-badge&logo=react&logoColor=white)
![HTML](https://img.shields.io/badge/HTML-%23E34F26.svg?style=for-the-badge&logo=html5&logoColor=white)
![CSS](https://img.shields.io/badge/CSS-%231572B6.svg?style=for-the-badge&logo=css3&logoColor=white)
![Node.js](https://img.shields.io/badge/Node.js-%23339933.svg?style=for-the-badge&logo=node.js&logoColor=white)

***Cubos Academy***

![](https://img.shields.io/badge/DDS%20Full%20Stack-%23147EFB.svg?style=for-the-badge&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABP0lEQVRIS+2W/Q3DIBSFv4I6UBQ2HjIVol0IXhBAgsyGNFQ+gSAsGAiEFAoLewR4aLmkM3f2l3v8+3Xmd7+/WY99rw5Obh4DGg8PtGnkbS4fRw0Dy/Txqf4Lc74RyIT9thvRwSOIuIsMWemfz5zHji+F39+PzPSv8G6u+P+1Gctn+ZH+8DF/yNk5K69GcS6+8S2D//3Bktv5T5RnHp0wDOQADkTA3M6TN/XKgVBjsL21CkH0vjRz3tTHyjEcrTyJIk4oRy/hzYbFLMQ4+QyD/cQVZ2wFtd7yzjIVd4iGoHrUqN4aB/7K4OQ4T08BZ3S6fElH0QffRkVdtPp/pRdrjTfk1z61TtJt/PR/7VbBkKcsW//7kD/v1c1Qo7hJLZy9O5tBz3I1Q6aivt3xe0++Szi0vqPj89m1z+jF3VN/oZJ3RqMt65K9tzeToLzL7L58Fy20ezp7nM+elVbDHS32U2f1bCUfKpgPxzl+mQk1xbSuxR3Up8bNJXNVFg1WxodZNy+utk/jxt6rbimXGyz5LVvmc7VHmd4zy6y5Hy+UHiclt5DazZn3OtcT21Pz70xuOQbdKzHD06TtWfOz3k+5z2Hv3RT6fFqJ1tc2v2xom1ejE4TRf03XeK3ZYnH+naVyK4yy/yuTwzE8fAAAAAElFTkSuQmCC)
![TypeScript](https://img.shields.io/badge/TypeScript-%233178C6.svg?style=for-the-badge&logo=typescript&logoColor=white)


### GitHub Stats ⚡
<div>
<a href="https://github.com/marvindev2022">
<img height="180em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=marvindev2022&layout=compact&langs_count=7&theme=dracula"/>
<img height="180
